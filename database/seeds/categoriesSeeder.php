<?php

use Illuminate\Database\Seeder;
use App\categories;

class categoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vendor_data = [
            ['name' => 'Sayuran'],
            ['name' => 'Minuman'],
            ['name' => 'Daging'],
        ];

        categories::insert($vendor_data);
    }
}
