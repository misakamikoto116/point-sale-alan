<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('index');
});
Route::resource('product', 'productController');
Route::resource('order', 'orderController');
Route::get('orders', 'productController@orders')->name('orders.list');