<div class="modal fade" id="pesanModal" tabindex="-1" role="dialog" aria-labelledby="pesanTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="pesanTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" id="prodId">
          <div class="form-group">
            <label for="qty">qty</label>
            <input type="text" name="qty" id="qty" class="form-control" placeholder="Qty" required>
          </div>

          <div class="form-group">
            <label for="prodPrice">Price</label>
            <input type="number" name="price" class="form-control" id="prodPrice" readonly>
          </div>

          <div class="form-group">
            <label for="prodPrice">Sub Total</label>
            <input type="number" name="price" class="form-control" id="sub_total" readonly>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>