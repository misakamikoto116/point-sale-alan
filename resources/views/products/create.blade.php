@extends('welcome')

@section('content')
  @if ($errors->any())
      @foreach ($errors->all() as $error)
          <div class="alert alert-danger">{{$error}}</div>
      @endforeach
  @endif
  <h2>Tambah Data Product</h2>
  <div class="mt-5"></div>
  
  <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" id="name" class="form-control" placeholder="Masukkan Nama" value="{{ old('name') }}" required>
    </div>

    <div class="form-group">
      <label for="stock">stock</label>
      <input type="number" name="stock" id="stock" class="form-control"  value="{{ old('stock') }}" placeholder="Masukkan stock" required>
    </div>

    <div class="form-group">
      <label for="price">Price</label>
      <input type="number" name="price" id="price" class="form-control" value="{{ old('price') }}" placeholder="Masukkan Price" required>
    </div>

    <div class="form-group">
      <label for="name">Category</label>
      <select class="select2 form-control" name="category" required>
        @foreach($categories as $category)
          <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="desc">Description</label>
      <textarea name="description" id="desc" placeholder="Masukkan Deskripsi" value="{{ old('description') }}" class="form-control"></textarea>
    </div>

    <div class="form-group">
      <label for="image">Image</label>
      <input type="file" name="img" id="image">
    </div>
  
    <div>
      <ul>
        <li>Ekstensi: jpeg, jpg, png</li>
      </ul>
    </div>

    <div class="form-group text-right">
      <button type="submit" class="btn btn-success">Simpan</button>
    </div>


  </form>
@endsection