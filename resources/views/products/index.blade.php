@extends('welcome')

@section('content')
  <div class="form-group text-right">
    <a href="{{ route('product.create') }}"><button class="btn btn-primary">Tambah Data</button></a>
  </div>
  <div class="mt-5"></div>
  <table class="table table-bordered table-striped">
    <thead class="thead-light">
      <tr>
        <th scope="col" width="1%">No</th>
        <th scope="col">name</th>
        <th scope="col">description</th>
        <th scope="col">stock</th>
        <th scope="col">price</th>
        <th scope="col">category</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $item)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item->name }}</td>
          <td>{{ $item->description }}</td>
          <td>{{ $item->stock }}</td>
          <td>{{ $item->price }}</td>
          <td>{{ $item->category->name }}</td>
          <td>
						<!-- <a href="">
							<button>Edit</button>
						</a>

						<a href="">
							<button>Hapus</button>
						</a> -->
					</td>
        </tr>
      @endforeach

    </tbody>
  </table>
  <div class="mt-5"></div>
  {{ $data->appends(request()->all())->links() }}
@endsection