@extends('welcome')

@section('content')
  @include('modal')
  <div class="mt-5"></div>
    @foreach($data as $item)
      @if ($item->products->count() > 0)
        <h2>{{ $item->name }}</h2>
        <div class="content bg-secondary">
            <div class="d-flex flex-row bd-highlight mb-3">
                @foreach($item->products as $prod)
                  <div class="p-2 bd-highlight">
                    <div class="card" style="width: 18rem;">
                      <div class="contener">
                        <img src="{{ asset('storage/products/' . $prod->image ) }}" class="card-img-top" style="height:300px;" alt="{{ $prod->name }}">
                        <div class="overlay">
                        <button class="btn btn-primary pesanBtn" data-title="{{ $prod->name }}" data-prodId="{{ $prod->id }}" data-price="{{ $prod->price }}">Pesan</button>
                        </div>
                      </div>
                      <div class="card-body">
                        <p class="card-text text-center">{{ $prod->name }}</p>
                      </div>
                    </div>
                  </div>
                @endforeach
            </div>
        </div>
      @endif
    @endforeach
    

    <!-- PART: orders Table -->
    @foreach($orders as $order)
      <table></table>
    @endforeach
    
  </div>
  <div class="mt-5"></div>
@endsection

@section('js')
  <script>
    $('.pesanBtn').click(function() {
      $('#pesanModal').modal('show');
      $('#pesanModal').find('#pesanTitle').html($(this).data('title'));
      $('#pesanModal').find('#prodId').val($(this).data('prodId'));
      $('#pesanModal').find('#prodPrice').val($(this).data('price'));
    });
  </script>
@endsection