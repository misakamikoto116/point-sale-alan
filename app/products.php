<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'stock',
        'price',
        'category_id',
        'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo(categories::class, 'category_id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->has('q')) {
            $query->where('nama', 'like', '%'.$request->get('q').'%');
        }


        return $query;
    }
}
