<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\products;
use App\categories;
use Illuminate\Support\Str;
use App\Http\Requests\ProductRequest;
use App\orders;


class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = products::Filter($request)->with('category')->paginate(10);
        $view = [
            'title' => 'Products',
            'data'  => $data
        ];

        return view('products.index')->with($view);
    }

    public function orders(Request $request)
    {
        $categories = categories::with('products')->get();
        $orders     = orders::with('order_details')->paginate(10);
        $view = [
            'title' => 'Products',
            'data'  => $categories,
            'orders'    => $orders
        ];

        return view('orders.index')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = categories::get();
        $view = [
            'categories' => $categories
        ];

        return view('products.create')->with($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        DB::beginTransaction(); /* PART: Begin transaction */

        try {
            $file = $request->img;
            $slug = Str::slug($file->getClientOriginalName(), '_');
            $photo = time() . $slug . '.' . $file->getClientOriginalExtension();
            products::create([
                'name'          => $request->name,
                'stock'         => $request->stock,
                'price'         => $request->price,
                'category_id'   => $request->category,
                'description'   => $request->description,
                'image'         => $photo
            ]);

            $this->SavePhotoNR($file, $photo, public_path('storage/products/'));

            DB::commit();

            // PART: Flash
            $request->session()->flash('messages', 'Berhasil Membuat data product');
            $request->session()->flash('type', 'success');

            return redirect()->route('product.index');
        } catch (\Exception $e) {
            DB::rollback();

            return $e->getmEssage();
        }

        
    }

    private function SavePhotoNR($image, $photo, $path)
    {
        $file = $image;
        $file->move($path, $photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
