<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string',
            'stock'         => 'required|numeric',
            'description'   => 'required|string',
            'img'         => 'required|image|mimes:jpeg,jpg,png,JPG,JPEG'
        ];
    }

    /**
     * return the message rules
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'   => 'Nama Barang harus diisi.',
            'stock.required'        => 'Nama Barang harus diisi.',
            'stock.numeric'         => 'Pilih lama bulan angsuran.',
            'description.required'  => 'Nama Lengkap harus diisi.',
            'img.required'        => 'Gambar harus dipilih.',
            'img.image'           => 'harus berupa gambar.',
            'img.mimes'           => 'ekstensien gambar tidak dikenal',
        ];

    }
}
